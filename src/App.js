import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Clock from "react-live-clock";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Demo deploy SPA to s3</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn Reactt
        </a>
        <div style={{ marginTop: 20 }}>
          <Clock
            format={"Y-M-D - HH:mm:ss:a"}
            ticking={true}
            timezone={"Asia/Ho_Chi_Minh"}
          />
        </div>
      </header>
    </div>
  );
}

export default App;
