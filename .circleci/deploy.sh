#!/bin/bash

if [ "${CIRCLE_BRANCH}" == "develop" ]
then
    echo "Deploy in branch develop"
    export BUCKET_NAME=demo-s3-develop
    export DISTRIBUTION_ID=""
elif [ "${CIRCLE_BRANCH}" == "staging" ]
then
    echo "Deploy in branch staging"
    export BUCKET_NAME=demo-s3-staging
    export DISTRIBUTION_ID=""
else
    echo "Deploy in branch production"
    export BUCKET_NAME=demo-s3-master
    export DISTRIBUTION_ID=""
fi

echo "Deploy to $BUCKET_NAME in branch ${CIRCLE_BRANCH}"
aws s3 sync build s3://$BUCKET_NAME --delete --exclude "*.sh"

if [ -z "$DISTRIBUTION_ID" ]
then
    echo "Not have Invalidation cloudfront $DISTRIBUTION_ID"
else
echo "Invalidation cloudfront $DISTRIBUTION_ID for branch ==> ${CIRCLE_BRANCH}"
    aws cloudfront create-invalidation --distribution-id "$DISTRIBUTION_ID" --paths "/*"
fi